[mod] visible wielded items [wieldview]
=======================================

Makes hand wielded items visible to other players.


Info for modders
################

Add items to the "no_wieldview" group with a raiting of 1 and it will not be shown by the wieldview.
